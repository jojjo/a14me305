<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Session;
/**
 * Groups Controller
 *
 * @property Group $Group
 */
class TwittersController extends AppController {

    private $oauth_consumer_key = 'UDjiEExZ8rm4dkeNdQ0NhOjDP';
    private $oauth_consumer_secret = 'FQcVOtMk8HfMStUwXWAuS7SyPqf7ZIijIoU8UL5GfHAOLvqiv5';

    private $oauth_signature_method = 'HMAC-SHA1';
    private $oauth_access_token_secret = ''; //omneocIDSG5PyCzXswdphrHLrmbRTSiRw35vvCvDCwwbF
    private $oauth_version = '1.0';
    private $twitter_host = 'https://api.twitter.com';
    private $twitter_upload_host = 'https://upload.twitter.com';

    public function generateUploadHeader($data, $oath_options)
    {
        $oath_options['oauth_signature'] = $this->generateUploadSignature($data, $oath_options);
        ksort($oath_options);
        $oauth_str = 'OAuth ';
        $count = count($oath_options);
        $i = 1;
        foreach ($oath_options as $k => $v)
        {
            $oauth_str .= rawurlencode($k). '="' . rawurlencode($v) . '"'. ($count == $i ? '' : ', ');
            $i++;
        }
        return $oauth_str;
    }

    public function generateUploadSignature($data, $oath_options)
    {
        if (!empty($data['oauth_access_token_secret'])){
            $oauth_access_token_secret = $data['oauth_access_token_secret'];
        }
        ksort($oath_options);
        $oauth_str = '';
        $count = count($oath_options);
        $i = 1;
        foreach ($oath_options as $k => $v)
        {
            $oauth_str .= rawurlencode($k) . '=' . rawurlencode($v) . ($count == $i ? '' : '&');
            $i++;
        }
        $base_url = $this->twitter_upload_host . $data['path'];
        $plain_txt = $data['method'] . '&' . rawurlencode($base_url) . '&' . rawurlencode($oauth_str);
        $signing_key = rawurlencode($this->oauth_consumer_secret) . '&' . (isset($oauth_access_token_secret) ? rawurlencode($oauth_access_token_secret) : '');
        $signature = base64_encode(hash_hmac('sha1', $plain_txt, $signing_key, true));
        return $signature;
    }

    public function generateHeader($data, $oath_options)
    {
        $oath_options['oauth_signature'] = $this->generateSignature($data, $oath_options);
        ksort($oath_options);
        $oauth_str = 'OAuth ';
        $count = count($oath_options);
        $i = 1;
        foreach ($oath_options as $k => $v)
        {
            $oauth_str .= rawurlencode($k). '="' . rawurlencode($v) . '"'. ($count == $i ? '' : ', ');
            $i++;
        }
        return $oauth_str;
    }

    public function generateSignature($data, $oath_options)
    {
        if (!empty($data['status']))
            $oath_options['status'] = $data['status'];

        if (!empty($data['media_ids']))
            $oath_options['media_ids'] = $data['media_ids'];

        if (!empty($data['in_reply_to_status_id']))
            $oath_options['in_reply_to_status_id'] = $data['in_reply_to_status_id'];

        if (!empty($data['oauth_access_token_secret'])){
            $oauth_access_token_secret = $data['oauth_access_token_secret'];
            debug($oauth_access_token_secret);

        }
        ksort($oath_options);
        $oauth_str = '';
        $count = count($oath_options);
        $i = 1;
        foreach ($oath_options as $k => $v)
        {
            $oauth_str .= rawurlencode($k) . '=' . rawurlencode($v) . ($count == $i ? '' : '&');
            $i++;
        }
        $base_url = $this->twitter_host . $data['path'];
        $plain_txt = $data['method'] . '&' . rawurlencode($base_url) . '&' . rawurlencode($oauth_str);
        $signing_key = rawurlencode($this->oauth_consumer_secret) . '&' . (isset($oauth_access_token_secret) ? rawurlencode($oauth_access_token_secret) : '');
        $signature = base64_encode(hash_hmac('sha1', $plain_txt, $signing_key, true));
        return $signature;
    }

    public function generateOAuthObject($oauth_access_token = null)
    {
        // Generate timestamp
        $time = time();

        // Generate nonce
        $nonce = $this->_randomString(32);

        // Build OAuth object
        $oauth_options = [
            'oauth_consumer_key' => $this->oauth_consumer_key,
            'oauth_nonce' => $nonce,
            'oauth_timestamp' => $time,
            'oauth_token' => $oauth_access_token,
            'oauth_version' => $this->oauth_version,
            'oauth_signature_method' => $this->oauth_signature_method,
        ];
        return $oauth_options;
    }

    /**
     * _base64URLEncode method
     *
     * @param array $data
     * @return string
     */
    function _base64URLEncode($data)
    {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    /**
     * _base64URLDecode method
     *
     * @param array $data
     * @return string
     */
    function _base64URLDecode($data)
    {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }

    /**
     * _random method
     *
     * @param array $length
     * @return string
     */
    function _randomString($length = 8)
    {
        $chars = 'bcdfghjklmnprstvwxzaeiou';

        $result = '';

        for ($p = 0; $p < $length; $p++)
        {
            $result .= ($p%2) ? $chars[mt_rand(19, 23)] : $chars[mt_rand(0, 18)];
        }

        return $result;
    }

    /**
     * _encrypt method
     *
     * @param array $value, $secretKey
     * @return string
     */
    function _encrypt($value, $secretKey)
    {
        return rtrim($value);
//            mcrypt_encrypt(
//                MCRYPT_RIJNDAEL_256,
//                $secretKey, $value,
//                MCRYPT_MODE_ECB,
//                mcrypt_create_iv(
//                    mcrypt_get_iv_size(
//                        MCRYPT_RIJNDAEL_256,
//                        MCRYPT_MODE_ECB
//                    ),
//                    MCRYPT_RAND)
//            ), "\0"
//        );
    }

    /**
     * _decrypt method
     *
     * @param array $value, $secretKey
     * @return string
     */
    function _decrypt($value, $secretKey)
    {
        return rtrim($value);
//            mcrypt_decrypt(
//                MCRYPT_RIJNDAEL_256,
//                $secretKey,
//                $value,
//                MCRYPT_MODE_ECB,
//                mcrypt_create_iv(
//                    mcrypt_get_iv_size(
//                        MCRYPT_RIJNDAEL_256,
//                        MCRYPT_MODE_ECB
//                    ),
//                    MCRYPT_RAND
//                )
//            ), "\0"
//        );
    }

    public function random()
    {
        $this->layout = 'ajax';

        $oauth_options = $this->generateOAuthObject($this->request->query['oauth_token']);
        $oauth_options['oauth_verifier'] = $this->request->query['oauth_verifier'];

        $data = [];
        $data['Threader']['path'] = '/oauth/access_token';
        $data['Threader']['method'] = 'POST';
        $data['Threader']['oauth_verifier'] = $this->request->query['oauth_verifier'];

        $result = $this->executeTwitterRequest($data, $oauth_options);

        if ($result['code'] == 200)
        {
            $randomIV = '1C3G5F7890123G56';
            $encrypted = $this->_base64URLEncode($this->_encrypt($result['body'], $randomIV));

            $session = $this->getRequest()->getSession();
            $session->write('threader', $encrypted);
            $this->redirect(['action' => 'upload']); //, $encrypted]);
        }
    }

    public function upload() {
        if ($this->request->is('post')) {
            $session = $this->getRequest()->getSession();
            $encrypted = $session->read('threader');
            $randomIV = '1C3G5F7890123G56';
            $decrypted = $this->_decrypt($this->_base64URLDecode($encrypted), $randomIV);
            debug($decrypted);

            preg_match('/(?<=oauth_token\=).*?(?=&)/', $decrypted, $oauth_token);
            preg_match('/(?<=oauth_token_secret\=).*?(?=&)/', $decrypted, $oauth_token_secret);
            preg_match('/(?<=screen_name\=).*?(?=&)/', $decrypted, $screen_name);

            //INIT
            $data = [];
            $data['Threader']['oauth_access_token_secret'] = $oauth_token_secret[0];
            $data['Threader']['path'] = '/1.1/media/upload.json';
            $data['Threader']['method'] = 'POST';
            $data['Threader']['command'] = 'INIT';
            $data['Threader']['media_type'] = $this->request->data['upload']['type'];
            $data['Threader']['total_bytes'] = $this->request->data['upload']['size'];

            $oauth_options = $this->generateOAuthObject($oauth_token[0]);
            $result = $this->handleTwitterUpload($data, $oauth_options);
            debug($result);
        }
    }

    public function threader($media_id)
    {
        $this->autoRender = false;
        /*if(!$this->Session->check('threader'))
        {
            return $this->redirect(['action' => 'login']);
        }*/
        $session = $this->getRequest()->getSession();
        $encrypted = $session->read('threader');
        $randomIV = '1C3G5F7890123G56';
        $decrypted = $this->_decrypt($this->_base64URLDecode($encrypted), $randomIV);

        preg_match('/(?<=oauth_token\=).*?(?=&)/', $decrypted, $oauth_token);
        preg_match('/(?<=oauth_token_secret\=).*?(?=&)/', $decrypted, $oauth_token_secret);

        if ($this->request->is('post'))
        {
            $this->request->data['Threader']['path'] = '/1.1/statuses/update.json';
            $this->request->data['Threader']['method'] = 'POST';

            $this->request->data['Threader']['status'] = 'Here\'s a pic!' ;
            $this->request->data['Threader']['media_ids'] = $media_id;

            $this->request->data['Threader']['oauth_access_token_secret'] = $oauth_token_secret[0];
            $oauth_options = $this->generateOAuthObject($oauth_token[0]);
            $response = $this->executeTwitterRequest($this->request->data, $oauth_options);

            $twitter_resp = json_decode($response['body']);
            $this->request->data['Threader']['in_reply_to_status_id'] = $twitter_resp->id;
            $this->redirect(['action' => 'upload']);
        }
    }

    private function finalize($json, $data, $oauth_options) {
        $path = $data['Threader']['path'];
        $method = $data['Threader']['method'];

        $ch = curl_init();
        $headers = array(
            'Content-Type: multipart/form-data',
            'Authorization: ' . $this->generateUploadHeader($data['Threader'], $oauth_options) // user:pass, base64 encoded
        );

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_URL, $this->twitter_upload_host . $path);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $finalizeData = array('command'=>'FINALIZE', 'media_id' => $json['media_id_string']);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $finalizeData);

        $response = curl_exec($ch);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($response, 0, $header_size);
        $body = substr($response, $header_size);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->threader($json['media_id_string']);
        curl_close($ch);
    }

    private function append($header, $data, $oauth_options)
    {
        //read video file
        $fp = fopen($this->request->data['upload']['tmp_name'], 'r');
        //segment counter
        $segmentId = 0;
        $uploadedBytes = 0;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);

        $json = json_decode($header, true);

        while (! feof($fp)) {
            $headers = array(
                'Content-Type: multipart/form-data',
                'Authorization: ' . $this->generateUploadHeader($data['Threader'], $oauth_options) // user:pass, base64 encoded
            );

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $data['Threader']['method']);
            curl_setopt($ch, CURLOPT_URL, $this->twitter_upload_host . $data['Threader']['path']);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $chunk = fread($fp, 50000);

            $uploadedBytes += strlen($chunk);

            $appenddata = array('command'=>'APPEND',
                'media_id' => $json['media_id_string'],
                'media' => $chunk,
                'segment_index' => $segmentId);

            curl_setopt($ch, CURLOPT_POSTFIELDS, $appenddata);
            $response = curl_exec($ch);

            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $header = substr($response, 0, $header_size);
            $body = substr($response, $header_size);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            $segmentId++;
        }
        $this->finalize($json, $data, $oauth_options);
        fclose($fp);
        curl_close($ch);
    }

    private function handleTwitterUpload($data, $oauth_options)
    {
        $path = $data['Threader']['path'];
        $method = $data['Threader']['method'];

        if(isset($data['Threader']['command']))
            $command = $data['Threader']['command'];
        if(isset($data['Threader']['media_type']))
            $media_type = $data['Threader']['media_type'];
        if(isset($data['Threader']['total_bytes']))
            $total_bytes = $data['Threader']['total_bytes'];

        $ch = curl_init();
        $headers = array(
            'Content-Type: multipart/form-data',
            'Authorization: ' . $this->generateUploadHeader($data['Threader'], $oauth_options) // user:pass, base64 encoded
        );

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_URL, $this->twitter_upload_host . $path);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $initdata = array('command' => $command, 'media_type' => $media_type, 'total_bytes' => $total_bytes);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $initdata);
        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($response, 0, $header_size);
        $body = substr($response, $header_size);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $result = ['code' => $httpcode, 'header' => $header, 'body' => $body];

        if($httpcode == 202) {
            curl_close($ch);
            $this->append($header, $data, $oauth_options);
        }
        return $result;
    }

    private function executeTwitterRequest($data, $oauth_options)
    {
        $path = $data['Threader']['path'];
        $method = $data['Threader']['method'];
        if(isset($data['Threader']['status']))
            $status = $data['Threader']['status'];
        if(isset($data['Threader']['media_ids']))
            $media_ids = $data['Threader']['media_ids'];
        if(isset($data['Threader']['in_reply_to_status_id']))
            $reply_to = $data['Threader']['in_reply_to_status_id'];
        if(isset($data['Threader']['oauth_callback']))
            $callback = $data['Threader']['oauth_callback'];
        if(isset($data['Threader']['oauth_verifier']))
            $verifier = $data['Threader']['oauth_verifier'];

        $ch = curl_init();
        $headers = array(
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: ' . $this->generateHeader($data['Threader'], $oauth_options) // user:pass, base64 encoded
        );

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_URL, $this->twitter_host . $path);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $post_data = '';
        if(!empty($status))
            $post_data .= '&status=' . urlencode($status);

        if(!empty($media_ids))
            $post_data .= '&media_ids=' . urlencode($media_ids);

        if(!empty($reply_to))
            $post_data .=  '&in_reply_to_status_id=' . urlencode($reply_to);

        if(!empty($callback))
            $post_data .=  '&oauth_callback=' . urlencode($callback);

        if(!empty($verifier))
            $post_data .=  '&oauth_verifier=' . urlencode($verifier);

        if(!empty($post_data))
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);

        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($response, 0, $header_size);
        $body = substr($response, $header_size);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $result = ['code' => $httpcode, 'header' => $header, 'body' => $body];

        curl_close($ch);
        return $result;
    }

    public function magiclogin()
    {
        $this->layout = 'ajax';
        $oauth_options = $this->generateOAuthObject();
        $oauth_options['oauth_callback'] = 'http://a1velander305.x10host.com/home/a1veland/public_html/twitters/random';

        $data['Threader']['path'] = '/oauth/request_token';
        $data['Threader']['method'] = 'POST';
        $data['Threader']['oauth_callback'] = 'http://a1velander305.x10host.com/home/a1veland/public_html/twitters/random';

        ksort($oauth_options);
        $result = $this->executeTwitterRequest($data, $oauth_options);

        if ($result['code'] == 200)
        {
            preg_match('/(?<=oauth_token\=).*?(?=&)/', $result['body'], $oauth_token);
            preg_match('/(?<=oauth_token_secret\=).*?(?=&)/', $result['body'], $oauth_token_secret);
            return $this->redirect('https://api.twitter.com/oauth/authenticate?oauth_token=' . $oauth_token[0]);
        }
    }
}