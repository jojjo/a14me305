<div class="jumbotron text-center">
    <h1>Upload your images to Twitter</h1>
    <p>Please log in to upload images or video to Twitter!</p>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <h1>Login</h1>
            <?= $this->Form->create() ?>
            <?= $this->Form->control('email') ?>
            <?= $this->Form->control('password') ?>
            <?= $this->Form->button('Login') ?>
            <?= $this->Form->end() ?>
        </div>
        <div class="col-sm-4">
            <h3>Register 2</h3>
           <p>Click <?php echo $this->Html->link('here', ['action' => 'add']);?> to create a user account</p>
        </div>
        <div class="col-sm-4">
            <h3>Join us.</h3>
            <p>Here you can upload images straight on to your twitter timeline!</p>
            <p><?=__('This is to demonstrate how you can translate a page.') ?></p>
        </div>
    </div>
</div>
