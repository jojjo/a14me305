<!--<form action="/cms/twitters/upload" method="post" enctype="multipart/form-data">-->
<!--    Select image to upload:-->
<!--    <input type="file" name="fileToUpload" id="fileToUpload">-->
<!--    <input type="submit" value="Upload Image" name="submit">-->
<!--</form>-->

<div class="jumbotron text-center">
    <h1>Upload your images here</h1>
    <p>Please chose a file from your computer!</p>
    <p><?php echo $this->Html->link('Logout', ['controller' => 'users', 'action' => 'login']); ?> </p>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <p>Click <?php echo $this->Html->link('here', ['action' => 'magicLogin']);?> to allow to post on twitter</p>
            <?php echo $this->Form->create( null,['url' => ['action' => 'upload'], 'type'=>'file']); ?>

            <?php //echo $this->Form->control('file', ['label' => 'Select a file']);
            echo $this->Form->input('upload', ['type' => 'file']);
            echo $this->Form->button('Update Details', ['class' => 'btn btn-lg btn-success1 btn-block padding-t-b-15']);
            echo $this->Form->end();
            ?>
        </div>
        <div class="col-sm-6">
            <h3>Join us.</h3>
            <p>Here you can upload images straight on to your twitter timeline!</p>
            <p><?=__('This is to demonstrate how you can translate a page.') ?></p>
        </div>
    </div>
</div>

