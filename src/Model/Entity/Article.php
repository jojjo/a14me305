<?php
/**
 * Created by PhpStorm.
 * User: johannavelander
 * Date: 2019-01-31
 * Time: 11:27
 */

// src/Model/Entity/Article.php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Article extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'slug' => false,
    ];
}
?>