<?php
/**
 * Created by PhpStorm.
 * User: johannavelander
 * Date: 2019-01-31
 * Time: 11:25
 */

// src/Model/Table/ArticlesTable.php
namespace App\Model\Table;

use Cake\ORM\Table;

class ArticlesTable extends Table
{
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');
    }
}
?>